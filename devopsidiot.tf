provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

#manually created keypair in AWS console
resource "aws_instance" "DevOpsIdiotv3" {
  ami           = "ami-024a64a6685d05041"
  instance_type = "t2.micro"
  key_name      = "DoI-Website-KeyPair"

  tags = {
    Name = "DevOpsIdiot-IaC-Terraform"
  }
}
